# Examples

## Brillouin Zone Critical Points

Let's pretend that we're solid state physicists.
In particular we'll pretend that we're solid state physicists looking at the [magnon dispersion in the zigzag phase of the Kitaev-Heisenber-``\Gamma`` model on a honeycomb lattice](https://journals.aps.org/prb/abstract/10.1103/PhysRevB.101.054424).
We've worked hard and figured out that the dispersion curve of this model is[^1]:

```@example brillouin
using LinearAlgebra

const UP = 1
const DOWN = -1

const S = 1 / 2
const J = -12
const K = 7
const Γ = 7
const J₃ = 3

const d₁ = [-1 / 2, sqrt(3) / 2]
const d₂ = [-1 / 2, -sqrt(3) / 2]
const d₃ = [1.0, 0.0]

const λ = S * (-J + 2K + 3J₃)
α(k) = S * (J + 3K / 4) * (exp(k ⋅ d₁ * im) + exp(k ⋅ d₂ * im))
β(k) = -S * K * exp(k ⋅ d₃ * im)
μ(k) =
    S * J * exp(k ⋅ d₃ * im) +
    S * J₃ * (exp(-2k ⋅ d₁ * im) + exp(-2k ⋅ d₂ * im) + exp(-2k ⋅ d₃ * im))
ν(k) = -1 / 4 * S * K * (exp(k ⋅ d₁ * im) + exp(k ⋅ d₂ * im))

ω(k, b1, b2) = sqrt(
    λ^2 + abs(α(k) + b2 * β(k))^2 - abs(μ(k) + b2 * ν(k))^2 +
    b1 * sqrt(
        2 * abs(α(k) + b2 * β(k))^2 * (2λ^2 - abs(μ(k) + b2 * ν(k))^2) +
        2 * real((α(k) + b2 * β(k))^2 * (conj(μ(k)) + b2 * conj(ν(k)))^2),
    ),
)
```

Great!
Now what?
Well we probably want to see what this dispersion curve looks like in the Brillouin zone.
We could just plot it on a 3-dimensional surface graph, but APS won't accept 3D graphs in their papers[^2]!
Well fortunately we solid state physicists have a solution for that!
We pick a few points that have interesting features, and plot the dispersion along the path that joins those points.
Now if only someone had written a library to make working with paths like this dead simple, maybe they could call it Meander...

Oh wait!
Let's use Meander to do exactly that!

```@example brillouin
using Meander

const N = 1000
const PATH = Path(
    N,
    [
        "X" => 2π / 3 * [0.0, sqrt(3)],
        "Γ" => 2π / 3 * [0.0, 0.0],
        "Y" => 2π / 3 * [1.0, 0.0],
        "Γ'" => 2π / 3 * [1.0, -sqrt(3)],
        "M" => 2π / 3 * [1.0, -sqrt(3)] / 2,
        "Γ" => 2π / 3 * [0.0, 0.0],
    ],
)

using CairoMakie

figure = Figure()
figure[1, 1] = Axis(figure, ylabel = "Energy (meV)", xticks = (indices(PATH), tags(PATH)))

lines!(figure[1, 1], 1:N, ω.(PATH, UP, UP), color = :red)
lines!(figure[1, 1], 1:N, ω.(PATH, UP, DOWN), color = :orange)
lines!(figure[1, 1], 1:N, ω.(PATH, DOWN, UP), color = :green)
lines!(figure[1, 1], 1:N, ω.(PATH, DOWN, DOWN), color = :blue)

save("dispersion.svg", figure)
```

Tada!

![](dispersion.svg)

[^1]: I would ``\LaTeX`` this but the website implements the equations in MathML so I have to copy it by hand, and sod writing it out twice... bloody APS...
[^2]: Bloody APS...
