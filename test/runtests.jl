using Meander
using Test

# TODO: Format tests.

@testset "Meander.jl" begin
    @testset "Path" begin
        # TODO: Test that these indices match the points.
        let path = Path(11, [:a => 0.0, :b => 10.0])
            @test indices(path) == [1, 11]
            @test tags(path) == [:a, :b]

            let steps = collect(path),
                points = Meander.points(path),
                indices = Meander.indices(path)

                @test steps[indices] == points == [0.0, 10.0]
            end
        end

        let path = Path(11, [:a => [0.0, 0], :b => [5.0, 0], :c => [5.0, 5]])
            @test indices(path) == [1, 6, 11]
            @test tags(path) == [:a, :b, :c]

            let steps = collect(path),
                points = Meander.points(path),
                indices = Meander.indices(path)

                @test steps[indices] == points == [[0.0, 0.0], [5.0, 0.0], [5.0, 5.0]]
            end
        end

        let path = Path(1000, [:a => [0.0, 5, 0], :b => [5.0, 5, 0], :c => [0.0, 0, 5]])
            @test indices(path) == [1, 367, 1000]
            @test tags(path) == [:a, :b, :c]

            let steps = collect(path),
                points = Meander.points(path),
                indices = Meander.indices(path)

                @test steps[indices] ==
                      points ==
                      [[0.0, 5.0, 0.0], [5.0, 5.0, 0.0], [0.0, 0.0, 5.0]]
            end
        end

        # TODO: This is the worst-case scenario of rounding issues that I can construct (it gives an absolute difference of 3), but I'm sure it could be even worse.
        #       We should figure out what the theoretical worst case scenario is and test that.
        let path = Path(
                22,
                [0 => 0.0, 1 => 1.0, 2 => 2.0, 3 => 3.0, 4 => 4.0, 5 => 5.0, 6 => 6.0],
            )
            @test indices(path) == [1, 5, 9, 13, 16, 19, 22]
            @test tags(path) == [0, 1, 2, 3, 4, 5, 6]

            let steps = collect(path),
                points = Meander.points(path),
                indices = Meander.indices(path)

                @test steps[indices] == points == [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0]
            end
        end

        let d = 2π / 3,
            Γ = [0, 0],
            M = d * [1, 0],
            K = d * [1, sqrt(3) / 3],
            path = Path(1000, ["Γ" => Γ, "M" => M, "K" => K, "Γ" => Γ])

            @test indices(path) == [1, 367, 578, 1000]
            @test tags(path) == ["Γ", "M", "K", "Γ"]

            let steps = collect(path),
                points = Meander.points(path),
                indices = Meander.indices(path)

                @test steps[indices] == points == [Γ, M, K, Γ]
            end
        end

        let d = 2π / 3,
            X = d * [0, sqrt(3)],
            Γ = [0, 0],
            Y = d * [1, 0],
            Γ! = d * [1, -sqrt(3)],
            M = d * [1 / 2, -sqrt(3) / 2],
            path =
                Path(1000, ["X" => X, "Γ" => Γ, "Y" => Y, "Γ'" => Γ!, "M" => M, "Γ" => Γ])

            @test indices(path) == [1, 269, 424, 692, 846, 1000]
            @test tags(path) == ["X", "Γ", "Y", "Γ'", "M", "Γ"]

            let steps = collect(path),
                points = Meander.points(path),
                indices = Meander.indices(path)

                @test steps[indices] == points == [X, Γ, Y, Γ!, M, Γ]
            end
        end

        @testset "Errors" begin
            @test_throws ErrorException collect(Path(1000, [:a => 0, :b => 10]))
        end

        @static if VERSION >= v"1.6"
            @testset "Regression" begin
                # These are just regression tests to make sure we don't change things by accident.
                # As such we only run them on the development version of Julia (currently v1.6) or later.
                # Only change them if you've intentionally changed the examples or the output of `show`.

                @testset "Examples" begin
                    include("examples/runtests.jl")
                end

                @testset "Printing" begin
                    let output = IOBuffer()
                        show(output, Path(1000, [:a => 0.0, :b => 10.0]))
                        @test String(take!(output)) == "a: 0.0 @ 1 => b: 10.0 @ 1000"
                    end

                    let output = IOBuffer()
                        show(
                            output,
                            MIME"text/plain"(),
                            Path(1000, [:a => 0.0, :b => 10.0]),
                        )
                        @test String(take!(output)) ==
                              "Path{Symbol, Float64}:\n   a: 0.0 @ 1\n=> b: 10.0 @ 1000"
                    end
                end
            end
        end
    end
end
