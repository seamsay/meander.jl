using CairoMakie
using LinearAlgebra
using Meander

const UP = 1
const DOWN = -1

const S = 1 / 2
const J = -12
const K = 7
const Γ = 7
const J₃ = 3

const d₁ = [-1 / 2, sqrt(3) / 2]
const d₂ = [-1 / 2, -sqrt(3) / 2]
const d₃ = [1.0, 0.0]

const λ = S * (-J + 2K + 3J₃)
α(k) = S * (J + 3K / 4) * (exp(k ⋅ d₁ * im) + exp(k ⋅ d₂ * im))
β(k) = -S * K * exp(k ⋅ d₃ * im)
μ(k) =
    S * J * exp(k ⋅ d₃ * im) +
    S * J₃ * (exp(-2k ⋅ d₁ * im) + exp(-2k ⋅ d₂ * im) + exp(-2k ⋅ d₃ * im))
ν(k) = -1 / 4 * S * K * (exp(k ⋅ d₁ * im) + exp(k ⋅ d₂ * im))

ω(k, b1, b2) = sqrt(
    λ^2 + abs(α(k) + b2 * β(k))^2 - abs(μ(k) + b2 * ν(k))^2 +
    b1 * sqrt(
        2 * abs(α(k) + b2 * β(k))^2 * (2λ^2 - abs(μ(k) + b2 * ν(k))^2) +
        2 * real((α(k) + b2 * β(k))^2 * (conj(μ(k)) + b2 * conj(ν(k)))^2),
    ),
)

const N = 1000
const PATH = Path(  # <1>
    N,
    [
        "X" => 2π / 3 * [0.0, sqrt(3)],
        "Γ" => 2π / 3 * [0.0, 0.0],
        "Y" => 2π / 3 * [1.0, 0.0],
        "Γ'" => 2π / 3 * [1.0, -sqrt(3)],
        "M" => 2π / 3 * [1.0, -sqrt(3)] / 2,
        "Γ" => 2π / 3 * [0.0, 0.0],
    ],
)

function plot(output)
    critical_point_indices = indices(PATH)  # <2>
    critical_point_tags = tags(PATH)  # <3>

    figure = Figure()
    figure[1, 1] = Axis(
        figure,
        ylabel = "Energy (meV)",
        xticks = (critical_point_indices, critical_point_tags),  # <4>
    )

    lines!(figure[1, 1], 1:N, ω.(PATH, UP, UP), color = :red)  # <5>
    lines!(figure[1, 1], 1:N, ω.(PATH, UP, DOWN), color = :orange)
    lines!(figure[1, 1], 1:N, ω.(PATH, DOWN, UP), color = :green)
    lines!(figure[1, 1], 1:N, ω.(PATH, DOWN, DOWN), color = :blue)

    save(output, figure)
end
