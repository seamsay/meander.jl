using Test

module Brillouin
include("brillouin/code.jl")
end

@testset "Examples" begin
    @testset "Brillouin Zone Critical Points" begin
        mktempdir() do directory
            # TODO: I couldn't get CairoMakie to write to a stream, but that would be better than creating a temporary directory.
            result_path = joinpath(directory, "output.png")
            Brillouin.plot(result_path)

            expected = read(joinpath(@__DIR__, "brillouin", "output.png"))
            actual = read(result_path)

            @test actual == expected
        end
    end
end
