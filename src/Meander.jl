"""
A module making it easier to define iterable paths through vector spaces.

* [`Path`](@ref)
* [`indices`](@ref)
* [`points`](@ref)
* [`tags`](@ref)

# Examples

```jldoctest
using LinearAlgebra

dispersion(k) = 0.5 + 1.5k ⋅ [0.0, 1.0] + 2.5k ⋅ [cos(-30π/180), sin(-30π/180)] + 3.5k ⋅ [cos(-150π/180), sin(-150π/180)]

path = Path(11, ["Γ" => [0.0, 0.0], "M" => [1.0, 0.0]])
dispersion.(path)

# output
11-element Vector{Float64}:
  0.5
  0.4133974596215561
  0.3267949192431122
  0.24019237886466815
  0.15358983848622443
  0.06698729810778059
 -0.01961524227066347
 -0.1062177826491073
 -0.19282032302755114
 -0.279422863405995
 -0.3660254037844388
```

# Glossary

A lot of the terms used in this documentation are ambiguous.
Therefore a glossary of how these terms will be used in this documentation follows.

* **Index/Indices**: An index refers to an index of the array obtained by calling `collect` on the `Path` iterator.
* **Path**: A path is some route through a space.
    The route is made up of sections that join points together with straight lines.
* **Point**: A point is a coordinate which _defines the path_.
    From a user's a point is one of the coordinates that you supply when defining the path.
* **Section**: A section is a straight line that joins two points.
* **Step**: A step is a coordinate along the path.
    When iterating through the path the elements of the iterator are steps (except the first element, which is the coordinate of the first point).
* **Tag**: A tag is the way that a particular point is referred to.
"""
module Meander

using LinearAlgebra

export indices, points, tags, Path

abstract type AbstractPath end

"""
    indices(path)

Return the indices of the points (see the glossary in the module level documentation).

# Examples

```jldoctest
julia> path = Path(10, [:a => 0.0, :b => 2.0, :c => 4.0]);

julia> collect(path)[indices(path)]
3-element Vector{Float64}:
 0.0
 2.0
 4.0
```
"""
function indices end

"""
    points(path)

Return the points that define the path.

# Examples

```jldoctest
julia> path = Path(10, [:a => 0.0, :b => 2.0, :c => 4.0]);

julia> points(path)
3-element view(::Vector{Float64}, :) with eltype Float64:
 0.0
 2.0
 4.0
```
"""
function points end

"""
    tags(path)

Return the tag for each point in the path.

# Examples

```jldoctest
julia> path = Path(10, [:a => 0.0, :b => 2.0, :c => 4.0]);

julia> tags(path)
3-element view(::Vector{Symbol}, :) with eltype Symbol:
 :a
 :b
 :c
```
"""
function tags end

"""
    Path(n, points)

A path through some abstract vector space.

* `n` is the number of coordinates that should make up the path iterator, i.e. `length(Path(n, points)) == n`.
* `points` are the coordinates in the space that define the path, see the glossary for more info.
    A point is provided to the constructor as a pair of `tag => vector`.
    `vector` does not necessarily have to be an array, see the examples.

The steps along the path of `Path` are *not necessarily equally spaced*, but this allows `Path` to ensure that every point will lie on a step (i.e. every point will appear when iterating through `Path`).
If equally spaced steps are required, [`EqualSpacePath`](@ref) can be used instead.

!!! warning "Integers"

    Generally the "inner" type of the vectors should be real numbers rather than integers.
    E.g. some valid vector types would be

    * Float64
    * Complex{Float64}
    * Vector{Float64}

    but not:

    * Int
    * Complex{Int}
    * Vector{Int}

    This is because of the way the types work out, and is hopefully just a limitation of the current implementation.

# Examples

Most of the time the vectors being used will be `Array` (or `StaticArray`, or similar)

```jldoctest
julia> Path(1000, ["Γ" => [0.0, 0.0], "M" => [1.0, 0.0], "K" => [1.0, sqrt(3)/3], "Γ" => [0.0, 0.0]])
Path{String, Vector{Float64}}:
   Γ: [0.0, 0.0] @ 1
=> M: [1.0, 0.0] @ 367
=> K: [1.0, 0.5773502691896257] @ 578
=> Γ: [0.0, 0.0] @ 1000
```

but it is also possible to use other kinds of "vectors":

```jldoctest
julia> Path(20, [:a => 0.0, :b => 1.0, :c => 10.0])
Path{Symbol, Float64}:
   a: 0.0 @ 1
=> b: 1.0 @ 3
=> c: 10.0 @ 20

julia> Path(9, [(0, 0) => 0.0 + 0.0im, (1, 0) => 1.0 + 0.0im, (1, 1) => 1.0 + 1.0im, (0, 1)=> 0.0 + 1.0im, (0, 0) => 0.0 + 0.0im])
Path{Tuple{Int64, Int64}, ComplexF64}:
   (0, 0): 0.0 + 0.0im @ 1
=> (1, 0): 1.0 + 0.0im @ 3
=> (1, 1): 1.0 + 1.0im @ 5
=> (0, 1): 0.0 + 1.0im @ 7
=> (0, 0): 0.0 + 0.0im @ 9
```

The primary use of `Path` is as an iterator:

```jldoctest
julia> path = Path(5, [:a => 0.0, :b => 1.0, :c => 3.0])
Path{Symbol, Float64}:
   a: 0.0 @ 1
=> b: 1.0 @ 2
=> c: 3.0 @ 5

julia> collect(path)
5-element Vector{Float64}:
 0.0
 1.0
 1.6666666666666665
 2.333333333333333
 3.0
```

Note that the points appear in the iterator at indices `1`, `2`, and `5`.
In fact

```jldoctest
julia> path = Path(1000, ["Γ" => [0.0, 0.0], "M" => [1.0, 0.0], "K" => [1.0, sqrt(3)/3], "Γ" => [0.0, 0.0]]);

julia> collect(path)[indices(path)] == points(path)
true
```

is true for all `Path`s.
"""
struct Path{T,V} <: AbstractPath
    indices::Vector{Int}
    points::Vector{V}
    tags::Vector{T}

    function Path(n::Int, points::Vector{Pair{T,V}}) where {T,V}
        @assert length(points) > 1
        @assert n >= length(points)

        steps = n - 1

        sections = Iterators.zip(
            Iterators.take(points, length(points) - 1),
            Iterators.drop(points, 1),
        )
        path_length = sum(norm(next - previous) for ((_, previous), (_, next)) in sections)

        sections_with_steps = [
            (
                steps = round(
                    Int,
                    steps * norm(next_coordinate - previous_coordinate) / path_length,
                ),
                tags = previous_tag => next_tag,
            ) for ((previous_tag, previous_coordinate), (next_tag, next_coordinate)) in
            sections
        ]

        final_points = [first(points).second]
        final_indices = [1]
        final_tags = [first(points).first]

        for ((tag, point), (steps, tags)) in
            zip(Iterators.drop(points, 1), sections_with_steps)
            @assert tags.first == last(final_tags)
            @assert tags.second == tag

            push!(final_points, point)
            push!(final_indices, last(final_indices) + steps)
            push!(final_tags, tag)
        end

        # Because of rounding issues we can end up with the index of the final point being significantly different than the requested number of indices.
        # To fix this we first adjust the number of indices in the final section by one (by changing the index of the final point), if that fixes the discrepancy we stop.
        # If it doesn't then we adjust the number of indices in the second to last section by one (by changing the index of the final point and the one before it), ...
        difference = n - last(final_indices)
        difference_sign = sign(difference)
        difference_abs = abs(difference)
        # TODO: There's got to be a better way to do this... Surely we can calculate `sections_with_steps` in such a way that we only need to fix the last index?
        index = lastindex(final_indices)
        # Greater than (rather than greater than or equal) because we don't want to change the first index ever.
        while index > firstindex(final_indices) && difference_abs > 0
            for i = index:lastindex(final_indices)
                final_indices[i] += difference_sign
            end

            difference_abs -= 1
            index -= 1
        end

        @assert n == last(final_indices) "I'm fairly sure the worst case scenario is that `difference` is still less than `length(points)`."

        new{T,V}(final_indices, final_points, final_tags)
    end
end

indices(path::Path) = view(path.indices, :)
points(path::Path) = view(path.points, :)
tags(path::Path) = view(path.tags, :)

Base.show(
    io::IO,
    path::Path;
    tag_suffix = ": ",
    index_prefix = " @ ",
    separator = " => ",
    prefix = "",
) = print(
    io,
    prefix,
    join(
        map(
            ((index, point, tag),) -> "$tag$tag_suffix$point$index_prefix$index",
            zip(indices(path), points(path), tags(path)),
        ),
        separator,
    ),
)
Base.show(io::IO, ::MIME"text/plain", path::Path) =
    show(io, path; separator = "\n=> ", prefix = "$(summary(path)):\n   ")

Base.iterate(path::Path) = (path.points[1], (section = 1 => 2, section_index = 2))

function Base.iterate(path::Path{T,V}, state) where {T,V}
    ((from_point_index, to_point_index), section_index) = state

    if from_point_index == lastindex(points(path))
        @assert to_point_index == lastindex(points(path)) + 1

        return nothing
    end

    from_index = indices(path)[from_point_index]
    to_index = indices(path)[to_point_index]
    from_point = points(path)[from_point_index]
    to_point = points(path)[to_point_index]

    section_steps = to_index - from_index
    if section_index > section_steps
        @assert section_index == section_steps + 1

        (to_point, (section = to_point_index => to_point_index + 1, section_index = 2))
    else
        section_spacing = (to_point - from_point) / section_steps
        coordinate = from_point + (section_index - 1) * section_spacing

        # TODO: Is there a way that we can figure types out automatically without the user having to worry?
        if !isa(coordinate, V)
            # NOTE: We don't use a type error here, because it introduces an incompatibility with Julia 1.0.
            error(strip("
Coordinate type of iteration does not match the coordinate type of the points given!

This probably means that you used integers in your coordinate type, e.g.:

    Path(1000, [:A => [0, 5, 0], :B => [6, 7, 0], :C => [8, 9, 10]])
"))
        end

        (
            coordinate,
            (
                section = from_point_index => to_point_index,
                section_index = section_index + 1,
            ),
        )
    end
end

Base.length(path::Path) = last(indices(path))
Base.eltype(::Type{Path{T,V}}) where {T,V} = V

# TODO: A constructor for `Path` that specifies the average spacing to be used and works out the indices based on that.
# TODO: A constructor for `Path` that takes a dictionary of tag => point and a list of points, so tha the user doesn't have to repeat themselves.
# TODO: `EqualSpacePath` an `AbstractPath` that ensures all steps are equally spaced, but does not guarantee that any of the points will be on a step.

end
