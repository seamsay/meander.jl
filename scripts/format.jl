using JuliaFormatter

project = dirname(dirname(@__FILE__))

format(project)

# Markdown files aren't actually formatted by `format` for some reason.
files = Iterators.filter(
    file -> last(splitext(file)) == ".md",
    (joinpath(root, file) for (root, _, files) in walkdir(project) for file in files),
)
for file in files
    @assert last(splitext(file)) == ".md"
    text = format_md(read(file, String))
    write(file, text)
end
