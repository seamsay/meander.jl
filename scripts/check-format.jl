using JuliaFormatter

project = dirname(dirname(@__FILE__))

files = collect(
    Iterators.filter(
        # TODO: Check Markdown files as well. Unfortunately `format` and `format_file` do not seem to format them, so fix that first.
        file -> last(splitext(file)) in [".jl", ".md"],
        (joinpath(root, file) for (root, _, files) in walkdir(project) for file in files),
    ),
)
width = maximum(length, files)

println("Checking Julia and Markdown files are formatted:")
results = map(files) do file
    print(' ', file, " "^(width - length(file) + 1), "... ")

    type = last(splitext(file))
    text = read(file, String)
    formatted = if type == ".jl"
        text == format_text(text)
    elseif type == ".md"
        text == format_md(text)
    else
        error("?!??")
    end

    if formatted
        println("✓")
    else
        println("❌")
    end

    formatted
end

if !all(results)
    error("Not all files formatted!")
end
